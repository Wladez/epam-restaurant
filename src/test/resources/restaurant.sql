-- -- -----------------------------------------------------
-- -- Table restaurant.roles
-- -- -----------------------------------------------------
DROP TABLE IF EXISTS roles ;
--
CREATE TABLE IF NOT EXISTS roles (
  id INT  NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  PRIMARY KEY (id));
---------------------------------------------------
-- Table restaurant.users
-- -----------------------------------------------------
DROP TABLE IF EXISTS users ;

CREATE TABLE IF NOT EXISTS users (
  id INT  NOT NULL AUTO_INCREMENT,
  role_id INT  NOT NULL,
  full_name VARCHAR(150) NULL DEFAULT NULL,
  email VARCHAR(150) NOT NULL,
  password VARCHAR(45) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_role
    FOREIGN KEY (role_id)
    REFERENCES roles (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;


DROP TABLE IF EXISTS dishes ;

CREATE TABLE dishes (id INT NOT NULL AUTO_INCREMENT,
                     title VARCHAR(150) NOT NULL,
                     cost DECIMAL(15,2) NOT NULL DEFAULT 0.00,
                     description MEDIUMTEXT NULL DEFAULT NULL,
                     is_vegeterian TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id));


-- -----------------------------------------------------
-- Table restaurant.orders
-- -----------------------------------------------------
DROP TABLE IF EXISTS orders ;

CREATE TABLE IF NOT EXISTS orders (
  id INT  NOT NULL AUTO_INCREMENT,
  user_id INT  NOT NULL,
  status ENUM('NEW', 'APPLIED', 'READY') NOT NULL DEFAULT 'NEW',
  is_paid TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
--   INDEX fk_user_orders_idx (user_id ASC),
  CONSTRAINT fk_user_orders
    FOREIGN KEY (user_id)
    REFERENCES users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

-- -- -----------------------------------------------------
-- -- Table restaurant.order_dishes
-- -- -----------------------------------------------------
DROP TABLE IF EXISTS order_dishes ;

CREATE TABLE IF NOT EXISTS order_dishes (
  order_id INT  NOT NULL,
  dish_id INT  NOT NULL,
  dish_amount TINYINT  NOT NULL DEFAULT 1,
  CONSTRAINT fk_orders
    FOREIGN KEY (order_id)
    REFERENCES orders (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE ,
  CONSTRAINT fk_dishel
    FOREIGN KEY (dish_id)
    REFERENCES dishes (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE )
;
