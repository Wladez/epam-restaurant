package restaurant.DAO;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import restaurant.AppConfig;
import restaurant.DAO.factory.ConnectionPool;
import restaurant.DAO.interfaces.IRoleDAO;
import restaurant.entities.Role;
import org.h2.tools.RunScript;
import org.junit.*;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@DirtiesContext
public class MySQLRoleDAOTest {
    @Autowired
    IRoleDAO roleDAO;

    @Before
    public void init() throws SQLException {
        String sqlTableCreate = "src/test/resources/restaurant.sql";
        Connection connection = ConnectionPool.getConnection();
        try {
            RunScript.execute(connection, new FileReader(sqlTableCreate));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        connection.close();
    }

    @Test
    public void create() {
        Role newRole = new Role();
        newRole.setName("new_role_name");
        int defaultId = newRole.getId();
        String roleName = newRole.getName();

        Role resultRole = roleDAO.create(newRole);
        Assert.assertNotNull("Result role is null", resultRole);
        Assert.assertEquals("Wrong results number",1, roleDAO.getAll().size());
        Assert.assertNotEquals("DB hasn't generated new key", defaultId, resultRole.getId());
        Assert.assertEquals("Names has changed", roleName, resultRole.getName());
        roleDAO.delete(newRole.getId());
    }

    @Test
    public void delete() {
        Role newRole = new Role();
        newRole.setName("new_role_name");
        roleDAO.create(newRole);

        Assert.assertTrue("Nothing has deleted", roleDAO.delete(newRole.getId()));
        Assert.assertEquals("Wrong results number",0, roleDAO.getAll().size());
    }

    @Test
    public void update() {
        Role newRole = new Role();
        newRole.setName("new_role_name");
        roleDAO.create(newRole);

        newRole.setName("changed_role");


        Assert.assertTrue("Update method returned false", roleDAO.update(newRole));
        Assert.assertEquals( "Doesn't equal to new role name","changed_role", roleDAO.getById(newRole.getId()).getName());
        roleDAO.delete(newRole.getId());
    }

    @Test
    public void getAll() {
        Role newRole1 = new Role();
        newRole1.setName("new_role_name1");
        Role newRole2 = new Role();
        newRole2.setName("new_role_name2");

        roleDAO.create(newRole1);
        roleDAO.create(newRole2);

        List<Role> resultList = roleDAO.getAll();
        Assert.assertNotNull("Result is null", resultList);
        Assert.assertEquals("Wrong results number",2, resultList.size());
        Assert.assertEquals(resultList.get(0), newRole1);
        Assert.assertEquals(resultList.get(1), newRole2);
        roleDAO.delete(newRole1.getId());
        roleDAO.delete(newRole2.getId());
    }

    @Test
    public void getById() {
        Role newRole = new Role();
        newRole.setName("new_role_name");
        roleDAO.create(newRole);

        Role resultRole = roleDAO.getById(newRole.getId());
        Assert.assertNotNull("Result is null", resultRole);
        Assert.assertEquals(resultRole, newRole);
        roleDAO.delete(newRole.getId());
    }
}