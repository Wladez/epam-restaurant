package restaurant.DAO;

import restaurant.AppConfig;
import restaurant.DAO.factory.ConnectionPool;
import restaurant.DAO.interfaces.IDishDAO;
import restaurant.entities.Dish;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@DirtiesContext
public class MySQLDishDAOTest {

    @Autowired
    IDishDAO dishDAO;

    int id;

    @Before
    public void init() throws Exception {
        String sqlTableCreate = "src/test/resources/restaurant.sql";
        Connection connection = ConnectionPool.getConnection();
        try {
            RunScript.execute(connection, new FileReader(sqlTableCreate));
//            RunScript.execute(connection, new FileReader(sqlFillTable));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Dish dish = new Dish();
        dish.setId(0);
        dish.setTitle("title1");
        dish.setCost(12.5);
        dish.setDescription("description1");
        dish.setVegeterian(true);
        dishDAO.create(dish);
        id = dish.getId();
        connection.close();
    }

    @Test
    public void create() throws SQLException {
        Dish dish = new Dish();
        dish.setId(0);
        dish.setTitle("title");
        dish.setCost(10.5);
        dish.setDescription("description");
        dish.setVegeterian(false);
        dishDAO.create(dish);
        assertNotNull("dish is null", dish);
        assertNotEquals("id is 0", 0, dish.getId());


    }

    @Test
    public void delete() {
        assertTrue("nothing is deleted", dishDAO.delete(id));
        assertFalse("wrong id", dishDAO.delete(99));
    }

    @Test
    public void update() {
        Dish dish = new Dish();
        dish.setId(id);
        dish.setTitle("title3");
        dish.setCost(15.5);
        dish.setDescription("description3");
        dish.setVegeterian(false);
        assertTrue("nothing is updated", dishDAO.update(dish));
        assertEquals("title is not equal 'title3'", "title3", dishDAO.getById(id).getTitle());
        dish.setId(99);
        assertFalse("something is updated", dishDAO.update(dish));
    }

    @Test
    public void getAll() {
        assertNotNull("dishes is null", dishDAO.getAll());
    }

    @Test
    public void getById() {
        assertNotEquals("title is null", null, dishDAO.getById(id).getTitle());
    }

}