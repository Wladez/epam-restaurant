package restaurant.DAO;

import restaurant.AppConfig;
import restaurant.DAO.factory.ConnectionPool;
import restaurant.DAO.interfaces.IOrderDAO;
import restaurant.entities.*;
import restaurant.entities.enums.OrderStatus;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;


import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.h2.tools.RunScript;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@DirtiesContext
public class MySQLOrderDAOTest {

    @Autowired
    private IOrderDAO orderDAO;

    @Before
    public void init() throws Exception {
        String initSqlScriptFileName = "/restaurant.sql";
        String dataSqlScriptFileName = "/restaurant_data.sql";
        Connection connection = ConnectionPool.getConnection();
        if (connection == null) System.exit(0);

        FileReader initSqlScriptFileReader =
                new FileReader(MySQLOrderDAOTest.class.getResource(initSqlScriptFileName).getFile());
        FileReader dataSqlScriptFileReader =
                new FileReader(MySQLOrderDAOTest.class.getResource(dataSqlScriptFileName).getFile());

        try {
            RunScript.execute(connection, initSqlScriptFileReader);
            RunScript.execute(connection, dataSqlScriptFileReader);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.closeConnection(connection);
            initSqlScriptFileReader.close();
            dataSqlScriptFileReader.close();
        }
    }

    @Test
    public void t010_create() throws Exception {
        User user = new User();
        user.setId(5);

        Map<Dish, Integer> dishes = new HashMap<>();
        Dish dish1 = new Dish();
        dish1.setId(1);
        Dish dish2 = new Dish();
        dish2.setId(2);

        dishes.put(dish1, 1);
        dishes.put(dish2, 2);

        Order order = new Order();
        order.setClient(user);
        order.setStatus(OrderStatus.NEW);
        order.setPaid(false);
        order.setDishes(dishes);

        Order returnedOrder = orderDAO.create(order);
        assertNotNull(returnedOrder);
        assertNotEquals(0, returnedOrder.getId());
    }

    @Test
    public void t020_deleteOrderWithExistingId() throws Exception {
        boolean result = orderDAO.delete(5);
        assertTrue(result);
    }

    @Test
    public void t021_deleteOrderWithNonExistedId() throws Exception {
        boolean result = orderDAO.delete(10);
        assertFalse(result);
    }

    @Test
    public void t014_updateOrderWithExistingId() throws Exception {
        User user = new User();
        user.setId(5);

        Map<Dish, Integer> dishes = new HashMap<>();
        Dish dish1 = new Dish();
        dish1.setId(6);
        Dish dish2 = new Dish();
        dish2.setId(5);
        dishes.put(dish1, 2);
        dishes.put(dish2, 1);

        Order order = new Order();
        order.setId(6);
        order.setClient(user);
        order.setStatus(OrderStatus.NEW);
        order.setDishes(dishes);

        boolean result = orderDAO.update(order);
        assertTrue(result);
    }

    @Test
    public void t015_updateOrderWithNonExistedIdId() throws Exception {
        User user = new User();
        user.setId(5);

        Map<Dish, Integer> dishes = new HashMap<>();
        Dish dish1 = new Dish();
        dish1.setId(6);
        Dish dish2 = new Dish();
        dish2.setId(5);
        dishes.put(dish1, 2);
        dishes.put(dish2, 1);

        Order order = new Order();
        order.setId(10);
        order.setClient(user);
        order.setStatus(OrderStatus.NEW);
        order.setDishes(dishes);

        boolean result = orderDAO.update(order);
        assertFalse(result);
    }

    @Test
    public void t016_updateOrderWithAdditionalDish() throws Exception {
        User user = new User();
        user.setId(5);

        Map<Dish, Integer> dishes = new HashMap<>();
        Dish dish1 = new Dish();
        dish1.setId(6);
        Dish dish2 = new Dish();
        dish2.setId(5);
        Dish dish3 = new Dish();
        dish3.setId(4);
        dishes.put(dish1, 2);
        dishes.put(dish2, 1);
        dishes.put(dish3, 3);

        Order order = new Order();
        order.setId(6);
        order.setClient(user);
        order.setStatus(OrderStatus.NEW);
        order.setDishes(dishes);

        boolean result = orderDAO.update(order);
        assertTrue(result);
    }

    @Test
    public void t017_updateOrderWithRemovingDish() throws Exception {
        User user = new User();
        user.setId(5);

        Map<Dish, Integer> dishes = new HashMap<>();

        Dish dish3 = new Dish();
        dish3.setId(4);

        dishes.put(dish3, 3);

        Order order = new Order();
        order.setId(6);
        order.setClient(user);
        order.setStatus(OrderStatus.APPLIED);
        order.setDishes(dishes);

        boolean result = orderDAO.update(order);
        assertTrue(result);
    }

    @Test
    public void t018_updateOrderWithNonexistentUser() throws Exception {
        User user = new User();
        user.setId(15);

        Map<Dish, Integer> dishes = new HashMap<>();

        Dish dish3 = new Dish();
        dish3.setId(4);

        dishes.put(dish3, 3);

        Order order = new Order();
        order.setId(6);
        order.setClient(user);
        order.setStatus(OrderStatus.APPLIED);
        order.setDishes(dishes);

        boolean result = orderDAO.update(order);
        assertFalse(result);
    }

    @Test
    public void t001_getAll() throws Exception {
        List<Order> orders = orderDAO.getAll();
        assertEquals(3,orders.size());
    }

    @Test
    public void t002_getByExistingId() throws Exception {
        Order receivedOrder = orderDAO.getById(4);
        assertNotNull(receivedOrder);
        assertEquals(3,receivedOrder.getDishes().size());
    }
    
    @Test
    public void t003_getByNonexistentId() throws Exception {
        Order receivedOrder = orderDAO.getById(10);
        assertNull(receivedOrder);
    }


    @Test
    public void t004_setAsIsPaidWithExistingId() throws Exception {
        boolean result = orderDAO.setAsIsPaid(5);
        assertTrue(result);
    }

    @Test
    public void t005_setAsIsPaidWithNonexistentId() throws Exception {
        boolean result = orderDAO.setAsIsPaid(10);
        assertFalse(result);
    }

    @Test
    public void t006_getOrdersByUserWithExistingUser() throws Exception {
        User client = new User();
        client.setId(5);

        List<Order> receivedOrders = orderDAO.getOrdersByUser(client);
        assertEquals(2, receivedOrders.size());
    }

    @Test
    public void t007_getOrdersByUserWithNonexistedUser() throws Exception {
        User client = new User();
        client.setId(10);

        List<Order> receivedOrders = orderDAO.getOrdersByUser(client);
        assertEquals(0, receivedOrders.size());
    }

    @Test
    public void t008_changeStatusWithExistingId() throws Exception {
        boolean result = orderDAO.changeStatus(4, OrderStatus.APPLIED);
        assertTrue(result);
    }

    @Test
    public void t008_changeStatusWithNonexistentId() throws Exception {
        boolean result = orderDAO.changeStatus(10, OrderStatus.NEW);
        assertFalse(result);
    }

}