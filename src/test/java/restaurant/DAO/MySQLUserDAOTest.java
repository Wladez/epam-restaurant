package restaurant.DAO;

import restaurant.AppConfig;
import restaurant.DAO.factory.ConnectionPool;
import restaurant.DAO.interfaces.IUserDAO;
import restaurant.entities.Role;
import restaurant.entities.User;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@DirtiesContext
public class MySQLUserDAOTest {

    @Autowired
    private IUserDAO userDAO;

    @Before
    public void init() throws Exception {
        String initSqlScriptFileName = "/restaurant.sql";
        String dataSqlScriptFileName = "/restaurant_data.sql";
        Connection connection = ConnectionPool.getConnection();
        if (connection != null) {

            FileReader initSqlScriptFileReader =
                    new FileReader(MySQLOrderDAOTest.class.getResource(initSqlScriptFileName).getFile());
            FileReader dataSqlScriptFileReader =
                    new FileReader(MySQLOrderDAOTest.class.getResource(dataSqlScriptFileName).getFile());

            try {
                RunScript.execute(connection, initSqlScriptFileReader);
                RunScript.execute(connection, dataSqlScriptFileReader);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                initSqlScriptFileReader.close();
                dataSqlScriptFileReader.close();
                ConnectionPool.closeConnection(connection);
            }
        }
    }

    @Test
    public void create() {

        User user1 = new User(0,
                new Role(1, "user"),
                "Ivan Ivanov",
                "ivanov@mail.ru",
                "123");
        User createdUser = userDAO.create(user1);
        assertNotNull(createdUser);
        String actualEmailValue = createdUser.getEmail();
        assertEquals(user1.getEmail(), actualEmailValue);
    }

    @Test
    public void delete() {
        assertTrue(userDAO.delete(4));
    }

    @Test
    public void update() {

        User user1 = new User(6,
                new Role(1, "user"),
                "Ivan Ivanov",
                "ivanov@mail.ru",
                "123");
        assertTrue(userDAO.update(user1));

    }

    @Test
    public void getAll() {

        List<User> users = userDAO.getAll();
        assertEquals(4, users.get(0).getId());
        assertEquals(5, users.get(1).getId());
        assertEquals(6, users.get(2).getId());
    }

    @Test
    public void getById() {

        User user = userDAO.getById(5);
        assertEquals(5, user.getId());
        assertEquals("Client_1", user.getFullName());
        assertEquals("client_1@restaurant.com", user.getEmail());
        assertEquals("1111", user.getPassword());
    }
 }