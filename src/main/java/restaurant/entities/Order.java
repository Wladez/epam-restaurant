package restaurant.entities;

import restaurant.entities.enums.OrderStatus;

import java.util.*;

public class Order {

    private int id;
    private User client;
    private Map<Dish, Integer> dishes;
    private OrderStatus status;
    private boolean paid;

    public Order() {
    }

    public Order(int id, User client, Map<Dish, Integer> dishes, OrderStatus status, boolean paid) {
        this.id = id;
        this.client = client;
        this.dishes = dishes;
        this.status = status;
        this.paid = paid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public Map<Dish, Integer> getDishes() {
        return dishes;
    }

    public void setDishes(Map<Dish, Integer> dishes) {
        this.dishes = dishes;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public double getOrderCost() {
        if (dishes.isEmpty()) return 0;
        double sum = 0;
        Iterator<Map.Entry<Dish, Integer>> iterator = dishes.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Dish, Integer> entry = iterator.next();
            sum = sum + entry.getKey().getCost() * entry.getValue();
        }
        return sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getId() == order.getId() &&
                isPaid() == order.isPaid() &&
                Objects.equals(getClient(), order.getClient()) &&
                Objects.equals(getDishes(), order.getDishes()) &&
                getStatus() == order.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getClient(), getDishes(), getStatus(), isPaid());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Order{");
        sb.append("id=").append(id);
        sb.append(", client=").append(client);
        sb.append(", dishes=").append(dishes);
        sb.append(", status=").append(status);
        sb.append(", paid=").append(paid);
        sb.append('}');
        return sb.toString();
    }
}
