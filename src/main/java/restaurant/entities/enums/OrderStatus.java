package restaurant.entities.enums;

public enum OrderStatus {
    NEW,
    APPLIED,
    READY;
}
