package restaurant.entities.enums;

public enum OrderStatuses {
    NEW, APPLIED, READY
}
