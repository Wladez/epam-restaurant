package restaurant.entities;

import java.util.*;

public class Dish {

    private int id;
    private String title;
    private double cost;
    private String description;
    private boolean is_vegeterian;

    public Dish() {
    }

    public Dish(int id, String title, double cost, String description, boolean is_vegeterian) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.description = description;
        this.is_vegeterian = is_vegeterian;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVegeterian() {
        return is_vegeterian;
    }

    public void setVegeterian(boolean is_vegeterian) {
        this.is_vegeterian = is_vegeterian;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dish)) return false;
        Dish dish = (Dish) o;
        return getId() == dish.getId() &&
                Double.compare(dish.getCost(), getCost()) == 0 &&
                isVegeterian() == dish.isVegeterian() &&
                Objects.equals(getTitle(), dish.getTitle()) &&
                Objects.equals(getDescription(), dish.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getCost(), getDescription(), isVegeterian());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Dish{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", cost=").append(cost);
        sb.append(", description='").append(description).append('\'');
        sb.append(", is_vegeterian=").append(is_vegeterian);
        sb.append('}');
        return sb.toString();
    }
}
