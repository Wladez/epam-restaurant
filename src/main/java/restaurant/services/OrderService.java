package restaurant.services;

import restaurant.DAO.interfaces.IOrderDAO;
import restaurant.entities.Order;
import restaurant.entities.User;
import restaurant.entities.enums.OrderStatus;
import restaurant.exceptions.*;

import java.util.*;

public class OrderService {

    private IOrderDAO orderDAO;

    public OrderService() {};

    public void setOrderDAO(IOrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public Order addOrder(Order order) {
        Order order1 = orderDAO.create(order);
        if (order1 == null) {
            throw new EntityCreateFailException("New order creating is fail!");
        }
        return order1;
    }

    public void updateOrder(Order order) {
        if (!orderDAO.update(order)) throw
                new EntityUpdateFailException("The order with " + order.getId() + " updating is fail!");
    }

    public void deleteOrder(int orderId) {

        Order order = orderDAO.getById(orderId);
        if (order == null) throw new OrderNotFoundException(orderId);


        if (order.getStatus() == OrderStatus.READY)
            throw new DeleteOrderBecauseStatusException(order.getStatus());

        orderDAO.delete(orderId);
    }

    public List<Order> getAllOrders() {
        return orderDAO.getAll();
    }

    public List<Order> getOrdersByUser(User client) {
        return orderDAO.getOrdersByUser(client);
    }

    public Order getOrderById(int id) {
        Order order = orderDAO.getById(id);
        if (order == null) throw new OrderNotFoundException(id);
        return order;
    }

    public void changeOrderStatus(int orderId, OrderStatus newStatus) {

        Order order = orderDAO.getById(orderId);
        if (order == null) throw new OrderNotFoundException(orderId);

        if (newStatus == OrderStatus.APPLIED && order.getStatus() != OrderStatus.NEW
                || newStatus == OrderStatus.READY && order.getStatus() != OrderStatus.APPLIED) {
            throw new IllegalOrderStatusException(order.getStatus(), newStatus);
        }

        if (!orderDAO.changeStatus(orderId, newStatus)) throw new ConnectionFailException();
    }

    public boolean setOrderAsPaid(int orderId) {
        return orderDAO.setAsIsPaid(orderId);
    }

}
