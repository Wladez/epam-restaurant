package restaurant.services;

import org.springframework.util.DigestUtils;
import restaurant.DAO.interfaces.IDishDAO;
import restaurant.DAO.interfaces.IOrderDAO;
import restaurant.DAO.interfaces.IRoleDAO;
import restaurant.DAO.interfaces.IUserDAO;
import restaurant.entities.Dish;
import restaurant.entities.Order;
import restaurant.entities.Role;
import restaurant.entities.User;
import restaurant.exceptions.EntityDeleteFailException;
import restaurant.exceptions.EntityNotFoundException;
import restaurant.entities.enums.OrderStatus;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Map;


public class UserService {

    private IUserDAO userDAO;
    private IDishDAO dishDAO;
    private IOrderDAO orderDAO;
    private IRoleDAO roleDAO;

    public UserService() {
    }

    public void setUserDAO(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void setDishDAO(IDishDAO dishDAO) {
        this.dishDAO = dishDAO;
    }

    public void setOrderDAO(IOrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public void setRoleDAO(IRoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }

    public List<User> getAllClients() {
        return userDAO.getAll().stream().filter((user) -> user.getRole().getName().equals("Client"))
                .collect(Collectors.toList());
    }
    public List<User> getAllUsers() {
        return userDAO.getAll(); }


    public boolean addUser(String textRoleId, String fullName, String email, String password) {

        if (textRoleId != null && !textRoleId.equals("") &&
                fullName != null && !fullName.equals("") &&
                email != null && !email.equals("") &&
                password != null && !password.equals("")) {

            int roleId = Integer.parseInt(textRoleId);
            Role role = roleDAO.getById(roleId);

            List<User> currentUsers = userDAO.getAll();
            for (User user : currentUsers) {
                if (user.getEmail().equals(email)) {
                    return false;
                }
            }

            User user = new User(0, role, fullName, email, passwordToHash(password));
            userDAO.create(user);
            return true;
        }
        return false;
    }


    public boolean deleteUser(String textUserId) {

        int userId = Integer.parseInt(textUserId);
        List<User> currentUsers = userDAO.getAll();
        for (User user : currentUsers) {
            if (user.getId() == userId) {
                userDAO.delete(userId);
                return true;
            }
        }
        return false;
    }

    public void deleteUser(int userId) {

        User user = userDAO.getById(userId);
        if (user == null) throw new EntityNotFoundException("User", userId);

        if (!userDAO.delete(userId)) throw new EntityDeleteFailException("Impossible to delete client with id=" + userId
                + "cause there are orders from this user.");

    }

    public User updateUser(String textUserId,
                              String textRoleId,
                              String fullName,
                              String email,
                              String password) {

        if (textUserId != null && !textUserId.equals("") &&
                textRoleId != null && !textRoleId.equals("") &&
                fullName != null && !fullName.equals("") &&
                email != null && !email.equals("") &&
                password != null && !password.equals("")) {

            int userId = Integer.parseInt(textUserId);
            int roleId = Integer.parseInt(textRoleId);
            Role role = roleDAO.getById(roleId);

            User newUser = new User(userId, role, fullName, email, passwordToHash(password));
            List<User> currentUsers = userDAO.getAll();
            for (User user : currentUsers) {
                if (user.getId() == userId) {
                    userDAO.update(newUser);
                    return newUser;
                }
            }
        }
        return null;
    }

    public User getUserByEmail(String email, String password) {

        List<User> currentUsers = userDAO.getAll();
        for (User user : currentUsers) {
            if (user.getEmail().equals(email) && user.getPassword().equals(passwordToHash(password))) {
                return user;
            }
        }
        return null;
    }

    public List<Order> getOrderListByUser(User user) {

        return orderDAO.getOrdersByUser(user);
    }

    public List<Dish> getDishList() {

        return dishDAO.getAll();
    }

    public boolean createOrder(Map<Dish, Integer> dishesInOrder, User user) {

        Order order = new Order(0, user, dishesInOrder, OrderStatus.NEW, false);
        return orderDAO.create(order) != null;
    }

    public User getUserById(int id) {
        return userDAO.getById(id);
    }

    public String passwordToHash(String password) {
        password = "banana" + password + "banana";
        return DigestUtils.md5DigestAsHex(password.getBytes());
    }
}