package restaurant.services;

import restaurant.DAO.interfaces.IDishDAO;
import restaurant.entities.Dish;
import restaurant.exceptions.EntityCreateFailException;
import restaurant.exceptions.EntityDeleteFailException;
import restaurant.exceptions.EntityUpdateFailException;

import java.util.List;

public class DishService {

    private IDishDAO dishDAO;

    public void setDishDAO(IDishDAO dishDAO) {
        this.dishDAO = dishDAO;
    }

    public List<Dish> getMenu() {
        return dishDAO.getAll();
    }

    public Dish addDish(String title, String cost, String description, String is_vegeterian) {
        if (title == null || cost == null || title.equals("") || cost.equals(""))
            throw new EntityCreateFailException("Title or Cost is't specified");
        Dish dish = new Dish();
        dish.setTitle(title);
        try {
            dish.setCost(Double.parseDouble(cost));
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Wrong value for cost");
        }
        dish.setDescription(description);
        if (is_vegeterian.equals("true,false")) dish.setVegeterian(true);
        else dish.setVegeterian(Boolean.parseBoolean(is_vegeterian));
        return dishDAO.create(dish);
    }

    public void deleteDish(int id) {
        if (!dishDAO.delete(id)) {
            throw new EntityDeleteFailException("Dish", id);
        }
    }

    public void updateDish(String id, String title, String cost, String description, String is_vegeterian) {
        if (title == null && cost == null)
            throw new EntityCreateFailException("Title or Cost is't specified");
        Dish dish = new Dish();
        dish.setId(Integer.parseInt(id));
        dish.setTitle(title);
        try {
            dish.setCost(Double.parseDouble(cost));
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Wrong value for cost");
        }
        dish.setDescription(description);
        if (is_vegeterian.equals("true,false")) dish.setVegeterian(true);
        else dish.setVegeterian(Boolean.parseBoolean(is_vegeterian));
        if (!dishDAO.update(dish)) {
            throw new EntityUpdateFailException("Dish update with id=" + id + " is failed");
        }
    }

    public Dish getDishById(int id) {
       return dishDAO.getById(id);
    }

}
