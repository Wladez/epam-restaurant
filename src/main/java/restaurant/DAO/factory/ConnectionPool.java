package restaurant.DAO.factory;

import java.sql.Connection;
import java.sql.SQLException;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class ConnectionPool {

    private static ComboPooledDataSource connectionPool;

    private ConnectionPool() {};

    public void setConnectionPool(ComboPooledDataSource connectionPool) {
        ConnectionPool.connectionPool = connectionPool;
    }

    public static Connection getConnection() {
        try {
            return connectionPool.getConnection();
        } catch (SQLException e) {
            //TODO: Log this exception
        }

        return null;
    }

    public static void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            //TODO: log exception
        }
    }

    public static void destroy() {
        connectionPool.close();
    }
}
