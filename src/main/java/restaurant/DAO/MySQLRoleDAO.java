package restaurant.DAO;

import org.slf4j.Logger;
import restaurant.DAO.factory.ConnectionPool;
import restaurant.DAO.interfaces.IRoleDAO;
import restaurant.entities.Role;
import restaurant.exceptions.ConnectionFailException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLRoleDAO implements IRoleDAO {
    private static final String CREATE_ROLE = "INSERT INTO roles (name) VALUES (?);";
    private static final String UPDATE_ROLE = "UPDATE roles SET name = ? WHERE roles.id = ?;";
    private static final String SELECT_ALL_ROLES = "SELECT id, name FROM roles;";
    private static final String SELECT_ROLE_BY_ID = "SELECT id, name FROM roles  WHERE roles.id = ?;";
    private static final String DELETE_ROLE = "DELETE FROM roles WHERE roles.id = ?;";

    private Logger logger;

    @Override
    public Role create(Role role) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            RuntimeException exp = new ConnectionFailException();
            logger.warn(exp.toString());
            throw exp;
        }

        try (PreparedStatement statement = connection.prepareStatement(CREATE_ROLE, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, role.getName());
            int result = statement.executeUpdate();
            if (result == 0) return null;
            try(ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    role.setId(resultSet.getInt(1));
                }
            }
            return role;
        } catch (SQLException e) {
            logger.warn(e.toString());
            return null;
        } finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public boolean delete(int id) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            RuntimeException exp = new ConnectionFailException();
            logger.warn(exp.toString());
            throw exp;
        }

        try (PreparedStatement statement = connection.prepareStatement(DELETE_ROLE);
        ) {
            statement.setInt(1, id);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.warn(e.toString());
            return false;
        } finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public boolean update(Role role) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            RuntimeException exp = new ConnectionFailException();
            logger.warn(exp.toString());
            throw exp;
        }

        try (PreparedStatement statement = connection.prepareStatement(UPDATE_ROLE);
        ) {
            statement.setString(1, role.getName());
            statement.setInt(2, role.getId());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.warn(e.toString());
            return false;
        } finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public List<Role> getAll() {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            RuntimeException exp = new ConnectionFailException();
            logger.warn(exp.toString());
            throw exp;
        }

        try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_ROLES);
             ResultSet resultSet = statement.executeQuery()
        ) {
            List<Role> roleList = new ArrayList<>();
            while (resultSet.next()) {
                Role role = new Role();
                role.setId(resultSet.getInt("id"));
                role.setName(resultSet.getString("name"));
                roleList.add(role);
            }
            return roleList;
        } catch (SQLException e) {
            logger.warn(e.toString());
            return null;
        } finally {
            ConnectionPool.closeConnection(connection);
        }
    }

    @Override
    public Role getById(int id) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            RuntimeException exp = new ConnectionFailException();
            logger.warn(exp.toString());
            throw exp;
        }

        try (PreparedStatement statement = connection.prepareStatement(SELECT_ROLE_BY_ID);
        ) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                Role role = new Role();
                if (resultSet.next()) {
                    role.setId(resultSet.getInt("id"));
                    role.setName(resultSet.getString("name"));
                }
                return role;
            }
        } catch (SQLException e) {
            logger.warn(e.toString());
            return null;
        } finally {
            ConnectionPool.closeConnection(connection);
        }
    }

}
