package restaurant.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import restaurant.DAO.factory.ConnectionPool;
import static restaurant.DAO.factory.ConnectionPool.closeConnection;
import restaurant.DAO.interfaces.IOrderDAO;
import restaurant.DAO.interfaces.IUserDAO;
import restaurant.entities.*;
import restaurant.entities.enums.OrderStatus;
import org.slf4j.Logger;
import restaurant.exceptions.ConnectionFailException;

import java.sql.*;
import java.util.*;



public class MySQLOrderDAO implements IOrderDAO {

    private static final String GET_ALL_ORDERS =
            "SELECT orders.id AS order_id, orders.user_id, orders.status, orders.is_paid, " +
                    "dishes.id AS dish_id, dishes.title, dishes.cost, dishes.description, dishes.is_vegeterian, " +
                    "order_dishes.dish_amount " +
                    "FROM orders " +
                    "INNER JOIN order_dishes ON order_dishes.order_id=orders.id " +
                    "INNER JOIN dishes ON order_dishes.dish_id=dishes.id;";
    private static final String GET_ORDER_BY_ID =
            "SELECT orders.id AS order_id, orders.user_id, orders.status, orders.is_paid, " +
                    "dishes.id AS dish_id, dishes.title, dishes.cost, dishes.description, dishes.is_vegeterian, " +
                    "order_dishes.dish_amount " +
                    "FROM orders " +
                    "INNER JOIN order_dishes ON order_dishes.order_id=orders.id " +
                    "INNER JOIN dishes ON order_dishes.dish_id=dishes.id " +
                    "WHERE order_id = ?;";
    private static final String GET_ORDERS_BY_USER =
            "SELECT orders.id, orders.user_id, orders.status, orders.is_paid " +
                    "FROM orders WHERE orders.user_id=?;";
    private static final String GET_DISHES_FROM_ORDER =
            "SELECT dishes.id, dishes.title, dishes.description, dishes.cost, dishes.is_vegeterian, order_dishes.dish_amount " +
                    "FROM order_dishes " +
                    "INNER JOIN dishes ON order_dishes.dish_id=dishes.id " +
                    "WHERE order_dishes.order_id = ?;";
    private static final String CREATE_ORDER = "INSERT INTO orders (user_id, status, is_paid) VALUES (?, ?, ?);";
    private static final String CREATE_ORDER_DISHES = "INSERT INTO order_dishes (order_id, dish_id, dish_amount) VALUES (?, ?, ?);";
    private static final String DELETE_ORDER = "DELETE FROM orders WHERE orders.id=?;";
    private static final String UPDATE_ORDER = "UPDATE orders SET user_id=?, status=?, is_paid=? WHERE orders.id=?;";
    private static final String DELETE_DISHES_FROM_ORDER = "DELETE FROM order_dishes WHERE order_id=?;";
    private static final String SET_ORDER_AS_PAID = "UPDATE orders SET orders.is_paid=1 WHERE orders.id=?;";
    private static final String CHANGE_ORDER_STATUS = "UPDATE orders SET orders.status=? WHERE orders.id=?;";


    private static final String ID = "orders.id";
    private static final String STATUS = "orders.status";
    private static final String USER_ID = "orders.user_id";
    private static final String IS_PAID = "orders.is_paid";

    private static final String DISH_ID = "dishes.id";
    private static final String DISH_TITLE = "dishes.title";
    private static final String DISH_DESC = "dishes.description";
    private static final String DISH_COST = "dishes.cost";
    private static final String DISH_IS_VEG = "dishes.is_vegeterian";

    private static final String DISH_AMOUNT = "order_dishes.dish_amount";

    @Autowired
    private Logger logger;

    private IUserDAO userDAO;

    public MySQLOrderDAO() {
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public void setUserDAO(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public Order create(Order order) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement statement = connection.prepareStatement(CREATE_ORDER,  Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, order.getClient().getId());
            statement.setString(2, order.getStatus().toString());
            statement.setBoolean(3, order.isPaid());

            int result = statement.executeUpdate();
            if (result == 0) return null;

            try (ResultSet rs = statement.getGeneratedKeys()) {
                if (rs.next()) {
                    order.setId(rs.getInt(1));
                } else return null;
            }

            if ( !addDishesToOrder(connection, order) ) return null;

        } catch (SQLException e) {
            logger.warn(e.toString());
            return null;
        } finally {
            closeConnection(connection);
        }


        return order;
    }

    private boolean addDishesToOrder(Connection connection, Order order) {
        if (order.getDishes().size() > 0) {
            try (PreparedStatement statement = connection.prepareStatement(CREATE_ORDER_DISHES)) {
                Iterator<Map.Entry<Dish, Integer>> iterator = order.getDishes().entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<Dish, Integer> entry = iterator.next();
                    statement.setInt(1, order.getId());
                    statement.setInt(2, entry.getKey().getId());
                    statement.setInt(3, entry.getValue());

                    if (statement.executeUpdate() == 0) return false;
                }
            } catch (SQLException e) {
                logger.warn(e.toString());
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean delete(int id) {

        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement statement = connection.prepareStatement(DELETE_ORDER)) {
            statement.setInt(1, id);
            int result = statement.executeUpdate();
            return result == 1;
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            closeConnection(connection);
        }

        return false;
    }

    @Override
    public boolean update(Order order) {

        if (order.getDishes().isEmpty()) return delete(order.getId());

        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement statement = connection.prepareStatement(UPDATE_ORDER)) {
            statement.setInt(1, order.getClient().getId());
            statement.setString(2,order.getStatus().toString());
            statement.setBoolean(3, order.isPaid());
            statement.setInt(4,order.getId());

            int result = statement.executeUpdate();
            if (result == 0) return false;

            try (PreparedStatement statement2 = connection.prepareStatement(DELETE_DISHES_FROM_ORDER)) {
                statement2.setInt(1, order.getId());

                int result2 = statement2.executeUpdate();
                if (result2 == 0) return false;
            }

            if ( !addDishesToOrder(connection, order) ) return false;

        } catch (SQLException e) {
            logger.warn(e.toString());
            return false;
        } finally {
            closeConnection(connection);
        }
        return true;
    }

    @Override
    public List<Order> getAll() {

        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        List<Order> orders = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            statement.execute(GET_ALL_ORDERS);
            try (ResultSet rs = statement.getResultSet()) {
                rs.next();
                while (!rs.isAfterLast()) {
                    Order order = new Order();
                    order.setId(rs.getInt("order_id"));
                    order.setClient(userDAO.getById(rs.getInt(USER_ID)));
                    order.setStatus(OrderStatus.valueOf(rs.getString(STATUS)));
                    order.setPaid(rs.getBoolean(IS_PAID));
                    Map<Dish, Integer> dishes = new HashMap<>();
                    order.setDishes(dishes);

                    do {
                        Dish dish = new Dish();
                        dish.setId(rs.getInt("dish_id"));
                        dish.setTitle(rs.getString(DISH_TITLE));
                        dish.setDescription(rs.getString(DISH_DESC));
                        dish.setCost(rs.getDouble(DISH_COST));
                        dish.setVegeterian(rs.getBoolean(DISH_IS_VEG));
                        int amount = rs.getInt(DISH_AMOUNT);
                        dishes.put(dish, amount);
                    } while (rs.next() && rs.getInt("order_id") == order.getId());
                    orders.add(order);
                }
            }
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            closeConnection(connection);
        }

        return orders;
    }

    private Map<Dish, Integer> getDishesFromOrder(Connection connection, Order order) {
        Map<Dish, Integer> dishes = new HashMap<>();
        try (PreparedStatement statement = connection.prepareStatement(GET_DISHES_FROM_ORDER)) {
            statement.setInt(1, order.getId());

            statement.execute();

            try (ResultSet rs = statement.getResultSet()) {
                while (rs.next()) {
                    Dish dish = new Dish();
                    dish.setId(rs.getInt(DISH_ID));
                    dish.setTitle(rs.getString(DISH_TITLE));
                    dish.setCost(rs.getDouble(DISH_COST));
                    dish.setDescription(rs.getString(DISH_DESC));
                    dish.setVegeterian(rs.getBoolean(DISH_IS_VEG));
                    int amount = rs.getInt(DISH_AMOUNT);
                    dishes.put(dish, amount);
                }
            }
        } catch (SQLException e) {
            logger.warn(e.toString());
        }

        return dishes;
    }

    @Override
    public Order getById(int id) {

        Connection connection = ConnectionPool.getConnection();

        Order order = null;

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement statement = connection.prepareStatement(GET_ORDER_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
            try (ResultSet rs = statement.getResultSet()) {
                if (rs.next()) {
                    order = new Order();
                    order.setId(rs.getInt("order_id"));
                    order.setClient(userDAO.getById(rs.getInt(USER_ID)));
                    order.setStatus(OrderStatus.valueOf(rs.getString(STATUS)));
                    order.setPaid(rs.getBoolean(IS_PAID));
                    Map<Dish, Integer> dishes = new HashMap<>();
                    order.setDishes(dishes);
                    do {
                        Dish dish = new Dish();
                        dish.setId(rs.getInt("dish_id"));
                        dish.setTitle(rs.getString(DISH_TITLE));
                        dish.setDescription(rs.getString(DISH_DESC));
                        dish.setCost(rs.getDouble(DISH_COST));
                        dish.setVegeterian(rs.getBoolean(DISH_IS_VEG));
                        int amount = rs.getInt(DISH_AMOUNT);
                        dishes.put(dish, amount);
                    } while (rs.next() && rs.getInt("order_id") == order.getId());
                }
            }
        } catch (SQLException e) {
            logger.warn(e.toString());
        }finally {
            closeConnection(connection);
        }

        return order;
    }

    @Override
    public List<Order> getOrdersByUser(User client) {

        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        List<Order> orders = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(GET_ORDERS_BY_USER)) {
            statement.setInt(1, client.getId());

            statement.execute();

            try (ResultSet rs = statement.getResultSet()) {
                while (rs.next()) {
                    Order order = new Order();
                    order.setId(rs.getInt(ID));
                    order.setClient(userDAO.getById(rs.getInt(USER_ID)));
                    order.setStatus(OrderStatus.valueOf(rs.getString(STATUS)));
                    order.setPaid(rs.getBoolean(IS_PAID));
                    order.setDishes(getDishesFromOrder(connection, order));

                    orders.add(order);
                }
            }

        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            closeConnection(connection);
        }

        return orders;

    }

    @Override
    public boolean setAsIsPaid(int id) {

        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement statement = connection.prepareStatement(SET_ORDER_AS_PAID)) {
            statement.setInt(1, id);
            int result = statement.executeUpdate();
            return result == 1;
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            closeConnection(connection);
        }

        return false;
    }

    @Override
    public boolean changeStatus(int orderID, OrderStatus newStatus) {

        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement statement = connection.prepareStatement(CHANGE_ORDER_STATUS)) {
            statement.setString(1,newStatus.toString());
            statement.setInt(2, orderID);
            int result = statement.executeUpdate();
            return result == 1;
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            closeConnection(connection);
        }

        return false;
    }
}