package restaurant.DAO;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import restaurant.DAO.interfaces.IUserDAO;
import restaurant.entities.Role;
import restaurant.entities.User;
import restaurant.DAO.factory.ConnectionPool;
import restaurant.exceptions.ConnectionFailException;

import static restaurant.DAO.factory.ConnectionPool.closeConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLUserDAO implements IUserDAO {


    private static final String CREATE_REQUEST =
            "INSERT INTO users (role_id, full_name, email, password) VALUES (?, ?, ?, ?)";
    private static final  String DELETE_REQUEST = "DELETE FROM users WHERE id=?";
    private static final  String UPDATE_REQUEST =
            "UPDATE users SET role_id=?, full_name=?, email=?, password=? WHERE id=?";

    private static final  String SELECT_ALL_REQUEST =
            "SELECT users.id, users.role_id, users.full_name, users.email, users.password, "+
            "roles.id, roles.name FROM users INNER JOIN roles ON users.role_id=roles.id";

    private static final  String SELECT_BY_ID_REQUEST =
            "SELECT users.id, users.role_id, users.full_name, users.email, users.password, "+
            "roles.id, roles.name FROM users INNER JOIN roles ON users.role_id=roles.id"+
            " WHERE users.id=";

    public MySQLUserDAO() {
    }

    @Autowired
    private Logger logger;

    @Override
    public User create(User user) {

        Connection connection = ConnectionPool.getConnection();
        if (connection != null) {
            try (PreparedStatement st = connection.prepareStatement(CREATE_REQUEST, Statement.RETURN_GENERATED_KEYS)) {
                st.setInt(1, user.getRole().getId());
                st.setString(2, user.getFullName());
                st.setString(3, user.getEmail());
                st.setString(4, user.getPassword());
                st.executeUpdate();
                return user;
            } catch (SQLException e) {
                logger.warn(e.toString());
            } finally {
                closeConnection(connection);
            }
        } else {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }
        return null;

    }

    @Override
    public boolean delete(int id) {

        Connection connection = ConnectionPool.getConnection();
        if (connection != null) {
            try (PreparedStatement st = connection.prepareStatement(DELETE_REQUEST, Statement.RETURN_GENERATED_KEYS)) {
                st.setInt(1, id);
                st.executeUpdate();
                return true;
            } catch (SQLException e){
                logger.warn(e.toString());
            } finally {
                closeConnection(connection);
            }
        } else {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }
        return false;
    }

    @Override
    public boolean update(User user) {

        Connection connection = ConnectionPool.getConnection();
        if (connection != null) {
            try (PreparedStatement st = connection.prepareStatement(UPDATE_REQUEST, Statement.RETURN_GENERATED_KEYS)) {
                st.setInt(1, user.getRole().getId());
                st.setString(2, user.getFullName());
                st.setString(3, user.getEmail());
                st.setString(4, user.getPassword());
                st.setInt(5, user.getId());
                st.executeUpdate();
                return true;
            } catch (SQLException e) {
                logger.warn(e.toString());
            } finally {
                closeConnection(connection);
            }
        } else {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }
        return false;
    }

    @Override
    public List<User> getAll() {

        Connection connection = ConnectionPool.getConnection();
        List<User> resultList = new ArrayList<>();
        if (connection != null) {
            try (PreparedStatement st = connection.prepareStatement(SELECT_ALL_REQUEST);
                 ResultSet rs = st.executeQuery()) {

                while (rs.next()) {
                    User user = new User(Integer.parseInt(rs.getString("id")),
                            new Role(Integer.parseInt(rs.getString("role_id")),
                                    rs.getString("name")),
                            rs.getString("full_name"),
                            rs.getString("email"),
                            rs.getString("password"));
                    resultList.add(user);
                }

                return resultList;

            } catch (SQLException e) {
                logger.warn(e.toString());
            } finally {
                closeConnection(connection);
            }
        } else {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }
        return new ArrayList<>();
    }

    @Override
    public User getById(int id) {

        Connection connection = ConnectionPool.getConnection();
        if (connection != null) {
            String selectByIdRequestWithId = SELECT_BY_ID_REQUEST + id;
            try (PreparedStatement st = connection.prepareStatement(selectByIdRequestWithId);
                 ResultSet rs = st.executeQuery()) {

                if (rs.next()) {
                    User user = new User(Integer.parseInt(rs.getString("id")),
                            new Role(Integer.parseInt(rs.getString("role_id")),
                                    rs.getString("name")),
                            rs.getString("full_name"),
                            rs.getString("email"),
                            rs.getString("password"));

                    return user;
                }

            } catch (SQLException e){
                logger.warn(e.toString());
            } finally {
                closeConnection(connection);
            }
        } else {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }
        return null;
    }
}
