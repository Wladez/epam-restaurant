package restaurant.DAO;

import org.slf4j.Logger;
import restaurant.DAO.factory.ConnectionPool;
import restaurant.DAO.interfaces.IDishDAO;
import restaurant.entities.Dish;
import restaurant.exceptions.ConnectionFailException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLDishDAO implements IDishDAO {

    private Logger logger;

    private static final String CREATE_SQL = "INSERT INTO dishes (title, cost, description, is_vegeterian)" +
            " VALUES (?,?,?,?)";
    private static final String DELETE_SQL = "DELETE FROM dishes WHERE id=?";
    private static final String INSERT_SQL = "UPDATE dishes" +
            " SET title=?, cost=?, description=?, is_vegeterian=? WHERE id=?;";
    private static final String GET_ALL_SQL = "SELECT id, title, cost, description, is_vegeterian FROM dishes;";
    private static final String GET_BY_ID = "SELECT id, title, cost, description, is_vegeterian" +
            " FROM dishes WHERE id=?;";

    @Override
    public Dish create(Dish entity) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement st = connection.prepareStatement(CREATE_SQL, Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, entity.getTitle());
            st.setDouble(2, entity.getCost());
            st.setString(3, entity.getDescription());
            st.setBoolean(4, entity.isVegeterian());
            int result = st.executeUpdate();
            try (ResultSet rs = st.getGeneratedKeys()) {
                rs.next();
                entity.setId(rs.getInt("id"));
            }
            if (result == 1) {
                return entity;
            } else {
                return null;
            }
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            ConnectionPool.closeConnection(connection);
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement st = connection.prepareStatement(DELETE_SQL)) {
            st.setInt(1, id);
            int result = st.executeUpdate();
            return result == 1;
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            ConnectionPool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public boolean update(Dish entity) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement st = connection.prepareStatement(INSERT_SQL)) {
            st.setString(1, entity.getTitle());
            st.setDouble(2, entity.getCost());
            st.setString(3, entity.getDescription());
            st.setBoolean(4, entity.isVegeterian());
            st.setInt(5, entity.getId());
            int result = st.executeUpdate();
            return result == 1;
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            ConnectionPool.closeConnection(connection);
        }
        return false;
    }

    @Override
    public List<Dish> getAll() {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement st = connection.prepareStatement(GET_ALL_SQL);
             ResultSet rs = st.executeQuery()) {
            List<Dish> dishes = new ArrayList<>();
            while (rs.next()) {
                Dish dish = new Dish();
                dish.setId(rs.getInt("id"));
                dish.setTitle(rs.getString("title"));
                dish.setCost(rs.getDouble("cost"));
                dish.setDescription(rs.getString("description"));
                dish.setVegeterian(rs.getBoolean("is_vegeterian"));
                dishes.add(dish);
            }
            return dishes;
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            ConnectionPool.closeConnection(connection);
        }
        return null;

    }

    @Override
    public Dish getById(int id) {
        Connection connection = ConnectionPool.getConnection();

        if (connection == null) {
            logger.error(new ConnectionFailException().getLocalizedMessage());
            throw new ConnectionFailException();
        }

        try (PreparedStatement st = connection.prepareStatement(GET_BY_ID)) {
            st.setInt(1, id);
            try (ResultSet rs = st.executeQuery()) {
                rs.next();
                Dish dish = new Dish();
                dish.setId(rs.getInt("id"));
                dish.setTitle(rs.getString("title"));
                dish.setCost(rs.getDouble("cost"));
                dish.setDescription(rs.getString("description"));
                dish.setVegeterian(rs.getBoolean("is_vegeterian"));
                return dish;
            }
        } catch (SQLException e) {
            logger.warn(e.toString());
        } finally {
            ConnectionPool.closeConnection(connection);
        }
        return null;
    }

}
