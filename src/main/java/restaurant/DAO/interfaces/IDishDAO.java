package restaurant.DAO.interfaces;

import restaurant.entities.Dish;

public interface IDishDAO extends IDAOBaseOperations<Dish> {

}
