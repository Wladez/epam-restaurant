package restaurant.DAO.interfaces;

import restaurant.entities.User;

public interface IUserDAO extends IDAOBaseOperations<User> {

}
