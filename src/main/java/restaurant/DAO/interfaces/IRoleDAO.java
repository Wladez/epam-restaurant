package restaurant.DAO.interfaces;

import restaurant.entities.Role;

public interface IRoleDAO extends IDAOBaseOperations<Role> {

}
