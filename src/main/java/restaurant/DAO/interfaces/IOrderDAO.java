package restaurant.DAO.interfaces;

import restaurant.entities.*;
import restaurant.entities.enums.OrderStatus;

import java.util.List;


public interface IOrderDAO extends IDAOBaseOperations<Order> {

    boolean setAsIsPaid(int id);

    boolean changeStatus(int orderID, OrderStatus newStatus);

    List<Order> getOrdersByUser(User client);
}
