package restaurant.controllers;

import restaurant.entities.enums.OrderStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import restaurant.services.OrderService;
import restaurant.services.UserService;

@Controller
public class OrderController {

    private OrderService orderService;
    private UserService userService;
    private UserController userController;
    public static final String REDIRECT_LOGIN = "redirect:/login";
    public static final String ADMINISTRATOR = "Administrator";

    public OrderController() {
    }

    ;

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/admin")
    public String showOrders(Model model) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            model.addAttribute("allOrdersList", orderService.getAllOrders());
            return "admin";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/admin/apply")
    public String changeOrderStatusToApplied(Model model, @RequestParam Integer id) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            try {
                orderService.changeOrderStatus(id, OrderStatus.APPLIED);
            } catch (RuntimeException e) {
                model.addAttribute("errorMessage", e.getLocalizedMessage());
                model.addAttribute("allOrdersList", orderService.getAllOrders());
                return "admin";
            }
            return "redirect:/admin";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/admin/ready")
    public String changeOrderStatusToReady(Model model, @RequestParam Integer id) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            try {
                orderService.changeOrderStatus(id, OrderStatus.READY);
            } catch (RuntimeException e) {
                model.addAttribute("errorMessage", e.getLocalizedMessage());
                model.addAttribute("allOrdersList", orderService.getAllOrders());
                return "admin";
            }
            return "redirect:/admin";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/admin/cancel")
    public String deleteOrder(Model model, @RequestParam Integer id) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            try {
                orderService.deleteOrder(id);
            } catch (RuntimeException e) {
                model.addAttribute("errorMessage", e.getLocalizedMessage());
                model.addAttribute("allOrdersList", orderService.getAllOrders());
                return "admin";
            }
            return "redirect:/admin";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/admin/users")
    public String showUsers(Model model) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            model.addAttribute("allUserList", userService.getAllClients());
            return "admin-users";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/admin/users/delete")
    public String deleteUser(Model model, @RequestParam Integer id) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {

        try {
            userService.deleteUser(id);
        } catch (RuntimeException e) {
            model.addAttribute("allUserList", userService.getAllClients());
            model.addAttribute("errorMessage", e.getLocalizedMessage());
            return "admin-users";
        }
        return "redirect:/admin/users";
    } else

    {
        return REDIRECT_LOGIN;
    }
}

    public void setUserController(UserController userController) {
        this.userController = userController;
    }

}
