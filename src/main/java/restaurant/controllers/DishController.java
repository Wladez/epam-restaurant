package restaurant.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import restaurant.services.DishService;

@Controller
public class DishController {

    private static final String REDIRECT_ADMIN_DISHES = "redirect:/admin/dishes";
    private static final String ADMIN_MENU = "admin-menu";
    private static final String ALL_DISHES_LIST = "allDishesList";
    private static final String ERROR_MESSAGE = "errorMessage";
    public static final String REDIRECT_LOGIN = "redirect:/login";
    public static final String ADMINISTRATOR = "Administrator";
    private DishService dishService;
    private UserController userController;

    public void setDishService(DishService dishService) {
        this.dishService = dishService;
    }

    @GetMapping("/admin/dishes")
    public String getMenu(Model model) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            model.addAttribute(ALL_DISHES_LIST, dishService.getMenu());
            return ADMIN_MENU;
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/admin/dishes/delete/{id}")
    public String deleteDish(@PathVariable int id, Model model) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            try {
                dishService.deleteDish(id);
            } catch (RuntimeException e) {
                model.addAttribute(ERROR_MESSAGE, e.getLocalizedMessage());
                model.addAttribute(ALL_DISHES_LIST, dishService.getMenu());
                return ADMIN_MENU;
            }
            model.addAttribute(ALL_DISHES_LIST, dishService.getMenu());
            return REDIRECT_ADMIN_DISHES;
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @PostMapping("/admin/dishes/add")
    public String addDish(@RequestParam String title,
                          @RequestParam String cost,
                          @RequestParam String description,
                          @RequestParam String is_vegeterian,
                          Model model) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            try {
                dishService.addDish(title, cost, description, is_vegeterian);
            } catch (RuntimeException e) {
                model.addAttribute(ERROR_MESSAGE, e.getLocalizedMessage());
                model.addAttribute(ALL_DISHES_LIST, dishService.getMenu());
                return ADMIN_MENU;
            }
            model.addAttribute(ALL_DISHES_LIST, dishService.getMenu());
            return REDIRECT_ADMIN_DISHES;
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @PostMapping("/admin/dishes/update")
    public String updateDish(@RequestParam String id,
                             @RequestParam String title,
                             @RequestParam String cost,
                             @RequestParam String description,
                             @RequestParam String is_vegeterian,
                             Model model) {
        if (userController.getAuthorizedUser() != null &&
                ADMINISTRATOR.equals(userController.getAuthorizedUser().getRole().getName())) {
            try {
                dishService.updateDish(id, title, cost, description, is_vegeterian);
            } catch (RuntimeException e) {
                model.addAttribute(ERROR_MESSAGE, e.getLocalizedMessage());
                model.addAttribute(ALL_DISHES_LIST, dishService.getMenu());
                return ADMIN_MENU;
            }
            model.addAttribute(ALL_DISHES_LIST, dishService.getMenu());
            return REDIRECT_ADMIN_DISHES;
        } else {
            return REDIRECT_LOGIN;
        }
    }
    public void setUserController(UserController userController) {
        this.userController = userController;
    }
}

