package restaurant.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import restaurant.entities.Dish;
import restaurant.entities.Order;
import restaurant.entities.User;
import restaurant.services.DishService;
import restaurant.services.OrderService;
import restaurant.services.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class UserController {
    private UserService userService;
    private OrderService orderService;
    private DishService dishService;
    public static final String REDIRECT_LOGIN = "redirect:/login";
    public static final String CLIENT = "Client";
    private User authorizedUser;

    private Map<Dish, Integer> currentDishMap = new HashMap<>();

    public UserController() {
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setAuthorizedUser(User authorizedUser) {
        this.authorizedUser = authorizedUser;
    }

    public void setDishService(DishService dishService) {
        this.dishService = dishService;
    }

    public User getAuthorizedUser() {
        return authorizedUser;
    }

    @GetMapping("/user")
    public String user(Model model) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            List<Order> orders = userService.getOrderListByUser(authorizedUser);
            model.addAttribute("currentUser", authorizedUser.getFullName());
            model.addAttribute("orderList", orders);
            return "user";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/user/userupdater")
    public String showUsers(Model model) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            model.addAttribute("currentUser", authorizedUser);
            return "userupdater";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/user/order")
    public String showDishes(Model model) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            model.addAttribute("dishList", userService.getDishList());
            model.addAttribute("currentUser", authorizedUser.getFullName());
            return "order";
        } else {
            return REDIRECT_LOGIN;
        }
    }


    @GetMapping("/login")
    public String login() {

        return "login";
    }

    @GetMapping("/login/usercreator")
    public String createUser() {
        return "usercreator";
    }

    @PostMapping("/login")
    public String logAsUser(@RequestParam String email,
                            @RequestParam String password,
                            Model model) {
        User currentUser = userService.getUserByEmail(email, password);
        if (currentUser != null) {
            setAuthorizedUser(currentUser);
            if (currentUser.getRole().getId() == 1) {
                return "redirect:/admin";
            }
            return "redirect:/user";
        }
        model.addAttribute("message", "Login or password is incorrect");
        return "login";
    }

    @GetMapping("/user/pay/{id}")
    public String changeOrderToPaid(@PathVariable Integer id) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            List<Order> orders = userService.getOrderListByUser(authorizedUser);
            for (Order order : orders) {
                if (order.getId() == id) {
                    orderService.setOrderAsPaid(id);
                    return "redirect:/user";
                }
            }
            return "home";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/user/cancel/{id}")
    public String cancelUser(Model model, @PathVariable Integer id) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            try {
                orderService.deleteOrder(id);
            } catch (RuntimeException e) {
                model.addAttribute("orderList", userService.getOrderListByUser(authorizedUser));
                model.addAttribute("errorMessage", e.getLocalizedMessage());
                return "user";
            }
            return "redirect:/user";
        } else {
            return REDIRECT_LOGIN;
        }

    }


    @PostMapping("/login/usercreator")
    public String createUserPost(@RequestParam String fullName,
                                 @RequestParam String email,
                                 @RequestParam String password,
                                 Model model) {

        if (userService.addUser("2", fullName, email, password)) {
            model.addAttribute("createMessage", "User is created successfully");
        } else {
            model.addAttribute("createMessage", "User is already exist");
        }
        return "usercreator";
    }


    @PostMapping("/user/userupdater/update")
    public String updateUser(@RequestParam String textId,
                             @RequestParam String textRoleId,
                             @RequestParam String fullName,
                             @RequestParam String email,
                             @RequestParam String password,
                             Model model) {


        authorizedUser = userService.updateUser(textId, textRoleId, fullName, email, password);
        return "redirect:/user";
    }

    @PostMapping("/user/order/create")
    public String createOrder(Model model) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            if (userService.createOrder(currentDishMap, authorizedUser)) {
                currentDishMap.clear();
            }
            return "redirect:/user";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/user/updateduser")
    public String showUpdatedUser(Model model) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            model.addAttribute("currentUser", userService.getUserById(authorizedUser.getId()));
            return "updateduser";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/user/order/adddish/{id}")
    public String addNewDishInOrderMap(Model model, @PathVariable Integer id) {
        if (this.getAuthorizedUser() != null &&
                CLIENT.equals(this.getAuthorizedUser().getRole().getName())) {
            if (!currentDishMap.containsKey(dishService.getDishById(id))) {
                currentDishMap.put(dishService.getDishById(id), 1);
            } else {
                currentDishMap.put(dishService.getDishById(id), currentDishMap.get(dishService.getDishById(id)) + 1);
            }
            model.addAttribute("dishList", userService.getDishList());
            model.addAttribute("currentDishMap", currentDishMap);
            return "order";
        } else {
            return REDIRECT_LOGIN;
        }
    }

    @GetMapping("/user/logout")
    public String userLogout() {
        this.setAuthorizedUser(null);
        return "redirect:/";
    }

}