package restaurant.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import restaurant.services.DishService;

@Controller
public class MenuController {
    private DishService dishService;

    public MenuController() {
    }

    public void setDishService(DishService dishService) {
        this.dishService = dishService;
    }

    @GetMapping("/menu")
    public String showDishes(Model model) {

        model.addAttribute("allDishesList", dishService.getMenu());
        return "menu";
    }
}
