package restaurant.exceptions;

public class EntityDeleteFailException extends RuntimeException {

    public EntityDeleteFailException(String message) {
        super(message);
    }

    public EntityDeleteFailException(String entityName, int entityId) {
        super("Impossible to delete " + entityName + " with id " + entityId + ".");
    }
}
