package restaurant.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String entityName, int entityId) {
        super(entityName + " with id " + entityId + "has not been found.");
    }
}
