package restaurant.exceptions;

//@ResponseStatus(HttpStatus.NOT_FOUND)
public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException(int orderId) {
        super("Error: The order with id=" + orderId + " has not been found.");
    }

    public OrderNotFoundException(String message) {
        super(message);
    }
}
