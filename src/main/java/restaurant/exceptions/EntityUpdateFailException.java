package restaurant.exceptions;

public class EntityUpdateFailException extends RuntimeException {
    public EntityUpdateFailException(String message) {
        super(message);
    }
}
