package restaurant.exceptions;

public class EntityCreateFailException extends RuntimeException {
    public EntityCreateFailException(String message) {
        super(message);
    }
}
