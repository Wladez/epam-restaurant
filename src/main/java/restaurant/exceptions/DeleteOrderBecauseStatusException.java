package restaurant.exceptions;

import restaurant.entities.enums.OrderStatus;

public class DeleteOrderBecauseStatusException extends RuntimeException {
    public DeleteOrderBecauseStatusException(OrderStatus orderStatus) {
        super("Impossible to delete order with status " + orderStatus);
    }
}
