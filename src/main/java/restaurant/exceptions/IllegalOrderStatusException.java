package restaurant.exceptions;

import restaurant.entities.enums.OrderStatus;

public class IllegalOrderStatusException extends RuntimeException {

    public IllegalOrderStatusException() {
        super("Impossible to change this order status.");
    }

    public IllegalOrderStatusException(OrderStatus oldOrderStatus, OrderStatus newOrderStatus) {
        super("Impossible to change order status from " + oldOrderStatus.name() + " to " + newOrderStatus.name());
    }
}
