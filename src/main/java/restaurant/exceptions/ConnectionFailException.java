package restaurant.exceptions;

public class ConnectionFailException extends RuntimeException {
    public ConnectionFailException() {
        super("Getting connection from the connection pool if fail.");
    }
}
