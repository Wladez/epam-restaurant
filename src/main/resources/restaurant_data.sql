﻿USE restaurant;
SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE roles;
INSERT INTO `roles` (`id`,`name`) VALUES (1,'Administrator');
INSERT INTO `roles` (`id`,`name`) VALUES (2,'Client');

TRUNCATE TABLE users;
INSERT INTO `users` (`id`,`role_id`,`full_name`,`email`,`password`) VALUES (4,1,'Admin','admin@restaurant.com','8f70e5687262de45e9f2ccd1e802d8fd');
INSERT INTO `users` (`id`,`role_id`,`full_name`,`email`,`password`) VALUES (5,2,'Client_1','client_1@restaurant.com','37cca58d857b721d2890951efecf3867');
INSERT INTO `users` (`id`,`role_id`,`full_name`,`email`,`password`) VALUES (6,2,'Client_2','client_2@restaurant.com','577f747a9fe51b4cb1f9d9b2a35a0bba');

TRUNCATE TABLE dishes;
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (1,'Паста карбонара',290.00,'Макароны с соусом карбонара',0);
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (2,'Суп «Мисо»',90.00,'Легкий суп на основе соевой пасты и рыбного бульона с добавлением сыра тофу, морских водорослей вакаме и свежего лука.',1);
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (3,'Солянка со сметаной',229.00,'Знаменитая сытная солянка с охотничьими колбасками, курицей, маслинами, маринованными огурчиками, лимоном, укропом, специями и сметаной.',0);
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (4,'Борщ с говядиной и сметаной',249.00,'Классический горячий борщ с отборной говядиной, овощами, зеленью, приправами и сметаной.',0);
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (5,'Салат «Цезарь» с креветками',349.00,'Аппетитная версия «Цезаря» с сочными тигровыми креветками, томатами черри, сыром пармезан, салатом айсберг и воздушными гренками.',0);
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (6,'WOK «Рис с овощами»',229.00,'Ароматный рассыпчатый рис с шампиньонами, болгарским красным перцем, морковью, репчатым и зеленым луком, белым кунжутом и соусом терияки.',1);
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (7,'Пицца «Маргарита»',407.00,'Классическая пицца, вкус которой определяют сыр моцарелла, свежие помидоры и томатный соус.',1);
INSERT INTO `dishes` (`id`,`title`,`cost`,`description`,`is_vegeterian`) VALUES (8,'Чизкейк клубничный',129.00,'Американский чизкейк на золотистом бисквите с начинкой из спелой клубники и сливочного сыр',1);

TRUNCATE TABLE orders;
INSERT INTO `orders` (`id`,`user_id`,`status`,`is_paid`) VALUES (4,5,'NEW',0);
INSERT INTO `orders` (`id`,`user_id`,`status`,`is_paid`) VALUES (5,6,'APPLIED',0);
INSERT INTO `orders` (`id`,`user_id`,`status`,`is_paid`) VALUES (6,5,'READY',1);

TRUNCATE TABLE order_dishes;
INSERT INTO `order_dishes` (`order_id`,`dish_id`,`dish_amount`) VALUES (4,1,1);
INSERT INTO `order_dishes` (`order_id`,`dish_id`,`dish_amount`) VALUES (4,3,1);
INSERT INTO `order_dishes` (`order_id`,`dish_id`,`dish_amount`) VALUES (4,8,1);
INSERT INTO `order_dishes` (`order_id`,`dish_id`,`dish_amount`) VALUES (5,2,2);
INSERT INTO `order_dishes` (`order_id`,`dish_id`,`dish_amount`) VALUES (5,4,1);
INSERT INTO `order_dishes` (`order_id`,`dish_id`,`dish_amount`) VALUES (6,6,1);

SET FOREIGN_KEY_CHECKS = 1;