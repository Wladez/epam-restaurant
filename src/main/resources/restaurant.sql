-- MySQL Script generated by MySQL Workbench
-- Fri May 11 12:13:23 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema restaurant
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `restaurant` ;

-- -----------------------------------------------------
-- Schema restaurant
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `restaurant` DEFAULT CHARACTER SET utf8 ;
USE `restaurant` ;

-- -----------------------------------------------------
-- Table `restaurant`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `restaurant`.`roles` ;

CREATE TABLE IF NOT EXISTS `restaurant`.`roles` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurant`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `restaurant`.`users` ;

CREATE TABLE IF NOT EXISTS `restaurant`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` INT UNSIGNED NOT NULL,
  `full_name` VARCHAR(150) NULL DEFAULT NULL,
  `email` VARCHAR(150) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_user_role_idx` (`role_id` ASC),
  CONSTRAINT `fk_user_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `restaurant`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurant`.`dishes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `restaurant`.`dishes` ;

CREATE TABLE IF NOT EXISTS `restaurant`.`dishes` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(150) NOT NULL,
  `cost` DECIMAL(15,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `description` MEDIUMTEXT NULL DEFAULT NULL,
  `is_vegeterian` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurant`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `restaurant`.`orders` ;

CREATE TABLE IF NOT EXISTS `restaurant`.`orders` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `status` ENUM('NEW', 'APPLIED', 'READY') NOT NULL DEFAULT 'NEW',
  `is_paid` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_user_orders_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_orders`
    FOREIGN KEY (`user_id`)
    REFERENCES `restaurant`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `restaurant`.`order_dishes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `restaurant`.`order_dishes` ;

CREATE TABLE IF NOT EXISTS `restaurant`.`order_dishes` (
  `order_id` INT UNSIGNED NOT NULL,
  `dish_id` INT UNSIGNED NOT NULL,
  `dish_amount` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  UNIQUE INDEX `unique_index` (`order_id` ASC, `dish_id` ASC),
  INDEX `fk_dishel_idx` (`dish_id` ASC),
  CONSTRAINT `fk_orders`
    FOREIGN KEY (`order_id`)
    REFERENCES `restaurant`.`orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_dishel`
    FOREIGN KEY (`dish_id`)
    REFERENCES `restaurant`.`dishes` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

DROP USER 'user'@'localhost';
CREATE USER 'user'@'localhost' IDENTIFIED BY 'Password1@';
GRANT ALL PRIVILEGES ON restaurant.* TO 'user'@'localhost';
FLUSH PRIVILEGES;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
